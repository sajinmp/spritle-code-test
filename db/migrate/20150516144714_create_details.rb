class CreateDetails < ActiveRecord::Migration
  def change
    create_table :details do |t|
      t.integer :contact_id
      t.string :address
      t.string :country

      t.timestamps null: false
    end
  end
end
