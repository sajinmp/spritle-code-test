class AddPhoneToDetails < ActiveRecord::Migration
  def change
    add_column :details, :phone, :string
  end
end
