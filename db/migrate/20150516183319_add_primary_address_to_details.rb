class AddPrimaryAddressToDetails < ActiveRecord::Migration
  def change
    add_column :details, :primary, :boolean, default: false
  end
end
