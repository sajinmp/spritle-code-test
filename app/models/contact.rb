class Contact < ActiveRecord::Base

  has_many :details, dependent: :destroy
  accepts_nested_attributes_for :details, allow_destroy: true

  before_save { self.email = email.downcase }
  valid_email_exp = /\A[\w+\-.]+@[a-z\d\-]+(\.[a-z]+)*\.[a-z]+\z/i
  validates :email, presence: true, format: { with: valid_email_exp }
  validates :fname, presence: true

end
