class Detail < ActiveRecord::Base

  belongs_to :contact
  validates :address, presence: true
  validates :phone, presence: true

end
