class ContactsController < ApplicationController
  def new
    @contact = Contact.new
    @contact.details.build
  end

  def create
    @contact = Contact.new(contact_params)
    if @contact.save
      flash[:success] = "Contact saved successfully"
      redirect_to(contacts_url)
    else
      render 'new'
    end
  end

  def index
    @contacts = Contact.all
  end

  def show
    @contact = Contact.find(params[:id])
  end

  private

  def contact_params
    params.require(:contact).permit(:fname, :lname, :email, details_attributes: [:id, :address, :phone, :country, :primary, :_destroy]) 
  end
end
